<?php

namespace App\Tests;

use App\Pesel\PeselValidator as PeselValidator;
use Exception;
use Monolog\Test\TestCase;

class PeselTests extends TestCase
{
    /**
     * @dataProvider invalidPeselNumbersForExceptions
     * @param $peselNumber
     * @throws Exception
     */
    public function peselExceptionTest($peselNumber) : void
    {
        $pesel = new PeselValidator($peselNumber);
    }

    /**
     * @dataProvider invalidPeselNumbers
     * @param $peselNumber
     * @throws Exception
     */
    public function peselTestWhenPeselNumberIsInvalidReturnFalse($peselNumber) : void
    {
        $pesel = new PeselValidator($peselNumber);
        $this->assertFalse($pesel->isPeselCorrect());
    }

    /**
     * @dataProvider correctPeselNumbersData
     * @param $peselNumber
     * @throws Exception
     */
    public function peselTestWhenPeselNumberIsValidReturnTrue($peselNumber) : void
    {
        $pesel = new PeselValidator($peselNumber);

        $getPeselNumber = $pesel->getPeselNumber();
        $this->assertTrue($pesel->isPeselCorrect());
        $this->assertEquals($getPeselNumber,$pesel->getPeselNumber());

        $this->assertInstanceOf(\DateTime::class,$pesel->getBirthdayFromPesel());
    }

    /**
     * @dataProvider futurePeselNumbersData
     * @param string $peselNumber
     * @throws Exception
     */
    public function testIsValidReturnsFalseWhenNumberIsInFutere($peselNumber) : void
    {
        $pesel = new PeselValidator($peselNumber);
        $this->assertFalse($pesel->isDateNowACorrectDate());
    }

    public function invalidPeselNumbersForExceptions() : array
    {
        return [
            [1234],
            ['1234'],
            ['alala'],
            ['a2a2a2a2'],

        ];
    }
    public function invalidPeselNumbersData() : array
    {
        return [
            ['00000000000'],
            ['11111111111'],
            ['97031003023'],
            ['97031003021'],
        ];
    }

    public function correctPeselNumbersData() : array
    {
        return [
            ["98071235179"],
            ["94032133429"],
            ["97070597587"],
            ["85042734131"],
            ["85041124168"]
        ];
    }

    public function futurePeselNumbersData() : array
    {
        return [
            ["27213113653"],
            ["27253198892"],
            ["25311377995"],
            ["36222382682"],
            ["26301053334"],
            ["32240121845"]
        ];
    }
}