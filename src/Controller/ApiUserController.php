<?php

namespace App\Controller;

use App\Entity\DTO\UserDTO;
use App\Form\UserEditType;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiUserController extends AbstractController
{
    /**
     * @Route("/", name="main_page")
     */
    public function homepage()
    {
        return $this->render('index.html.twig', [
        ]);
    }

    /**
     * @Route("/users", name="users_list")
     * @param Request $request
     * @return Response
     * @throws GuzzleException
     */
    public function getUserList(Request $request)
    {
        $api_path = $this->getParameter('apiurl');
        $client = new Client(['base_uri' => $api_path]);
        $request = $client->request(
            'GET',
            'users'
        );
        $request = json_decode($request->getBody(), null);

        return $this->render('users/userList.html.twig', [
            'users' => $request,
        ]);
    }

    /**
     * @Route("/search", name="search_user")
     * @param Request $request
     * @return Response
     */
    public function searchUser(Request $request)
    {
        $form = $this->createFormBuilder()
            ->setMethod('GET')
            ->add('query', TextType::class, [
                'label' => false])
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->get('query')->getData();

            return $this->redirectToRoute('searchUsersList', ['data' => $data]);
        }
        return $this->render('users/search.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/user_add", name="user_add")
     * @param Request $request
     * @return Response
     * @throws GuzzleException
     */
    public function addUser(Request $request)
    {
        $form = $this->createFormBuilder()
            ->setMethod('GET')
            ->add('name', TextType::class)
            ->add('gender', ChoiceType::class, [
                'choices' => [
                    'Male' => 'Male',
                    'Female' => 'Female'
                ]
            ])
            ->add('status', ChoiceType::class, [
                'choices' => [
                    'Active' => 'Active',
                    'Inactive' => 'Inactive'
                ]
            ])
            ->add('email', TextType::class)
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $api_path = $this->getParameter('apiurl');
            $access_token = $this->getParameter('token');
            $client = new Client(['base_uri' => $api_path]);
            $request = $client->request(
                'POST',
                'users',
                ['headers' =>
                    ['Authorization' => "Bearer {$access_token}"],
                    'form_params' => $data
                ]
            );
            $response = $request ? $request->getBody()->getContents() : null;
            $status = $request ? $request->getStatusCode() : 500;

            if ($response && $status === 200 && $response !== 'null') {
                $this->addFlash('success', 'User Created!');
                return $this->redirectToRoute('users_list', [
                    'data' => $data,
                ]);
            }
        }
        return $this->render('users/addUserForm.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/search_users_list/{data}", name="searchUsersList")
     * @param $data
     * @return Response
     * @throws GuzzleException
     */
    public function searchUserList($data)
    {
        if ($data !== null) {
            $api_path = $this->getParameter('apiurl');
            $client = new Client(['base_uri' => $api_path]);
            $request = $client->request(
                'GET',
                'users?name=' . $data
            );
            $request = json_decode($request->getBody(), null);

            return $this->render('users/searchList.html.twig', [
                'users' => $request,
            ]);
        }
        return $this->redirectToRoute('search_user');
    }

    /**
     * @Route("/updateUser/{userId}", name="updateUser")
     * @param Request $request
     * @param $userId
     * @return Response
     * @throws GuzzleException
     */
    public function updateUser(Request $request, $userId)
    {
        if ($userId !== null) {
            $api_path = $this->getParameter('apiurl');
            $client = new Client(['base_uri' => $api_path]);
            $apiRequest = $client->request(
                'GET',
                'users/' . $userId,
            );
            $apiRequest = json_decode($apiRequest->getBody()->getContents(), true);
            $data = $apiRequest['data'];
            $formData = [
                'name' => $data['name'],
                'gender' => $data['gender'],
                'status' => $data['status'],
                'email' => $data['email']
            ];

            $form = $this->createForm(UserEditType::class, $formData);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $userData = $form->getData();;

                $api_path = $this->getParameter('apiurl');
                $access_token = $this->getParameter('token');
                $client = new Client(['base_uri' => $api_path]);
                $request = $client->request(
                    'PATCH',
                    'users/' . $userId,
                    ['headers' =>
                        ['Authorization' => "Bearer {$access_token}"],
                        'form_params' => $userData
                    ]
                );
                return $this->redirectToRoute('users_list');
            }
        }
        return $this->render('users/addUserEditForm.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/removeUser/{userId}", name="removeUser")
     * @param $userId
     * @return Response
     * @throws GuzzleException
     */
    public function removeUser($userId)
    {
        if ($userId !== null) {
            $api_path = $this->getParameter('apiurl');
            $access_token = $this->getParameter('token');
            $client = new Client(['base_uri' => $api_path]);
            $request = $client->request(
                'DELETE',
                'users/' . $userId,
                ['headers' =>
                    ['Authorization' => "Bearer {$access_token}"]
                ]
            );
            $status = $request ? $request->getStatusCode() : 204;
            if ($status === 200)
                return $this->redirectToRoute('users_list');
        }
        return $this->redirectToRoute('search_user');
    }
}