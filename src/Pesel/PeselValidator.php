<?php


namespace App\Pesel;


use Exception;

final class PeselValidator
{
    /**
     * PeselValidator lenght maximum
     * @var int
     */
    const PESEL_MAX_LENGTH = 11;

    /**
     * PESEL WEIGHT for each PESEL number
     * @var array
     */
    const PESEL_WEIGHT = [1, 3, 7, 9, 1, 3, 7, 9, 1, 3, 1];

    /**
     * Years for check
     * @var array
     */
    const YEAR = ['1800','1900','2020','2100','2200'];

    /**
     * PESEL Number
     * @var int
     */
    private $peselNumber;

    /**
     * MAX year for check
     *
     * @var \DateTime
     */
    private $maxYearCheck;

    /**
     * MIN year for check
     *
     * @var \DateTime
     */
    private $minYearCheck;

    /**
     *
     * @param string $peselNumber
     * @throws \Exception
     */
    public function __construct(string $peselNumber)
    {
        $this->peselNumber = $peselNumber;

        $this->minYearCheck = new \DateTime('1900-01-01');
        $this->maxYearCheck = new \DateTime();

        if(!$this->isPeselLenghtCorrect()){
            throw new \Exception("PESEL length is not correct", 1);
        }

        if(!$this->isPeselANumber() ){
            throw new \Exception("PESEL must be a number", 1);
        }
    }

    /**
     * Checking PESEL lenght
     * @return bool
     */
    public function isPeselLenghtCorrect() : bool
    {
        return self::PESEL_MAX_LENGTH == strlen($this->peselNumber);
    }

    /**
     * Checking PESEL as a only number digits
     * @return bool
     */
    public function isPeselANumber() : bool
    {
        return ctype_digit($this->peselNumber);
    }

    /**
     * Get PESEL number
     * @return int
     */
    public function getPeselNumber() : int
    {
        return $this->peselNumber;
    }

    /**
     * Return birthday from correct PESEL
     * @return \DateTime
     * @throws \Exception
     */
    public function getBirthdayFromPesel() : \DateTime
    {
        $yearOfBirth = $this->getYear();
        $monthOfBirth = $this->getMonth();
        $dayOfBirth =  $this->peselNumber[4].$this->peselNumber[5];

        return new \DateTime($yearOfBirth."-".$monthOfBirth."-".$dayOfBirth);
    }

    /**
     * Return month from correct PESEL
     *
     * @return string
     */
    private function getMonth() : string
    {
        return ($this->peselNumber[2] % 2).$this->peselNumber[3];
    }

    /**
     * Return year from correct PESEL
     * @return string
     */
    private function getYear() : string
    {
        return self::YEAR[floor($this->peselNumber[2]/2)] . ($this->peselNumber[0] . $this->peselNumber[1]);
    }

    /**
     * Valid is not future PESEL
     * @return bool
     * @throws Exception
     */
    public function isDateNowACorrectDate() : bool
    {
        return (new \DateTime()) > $this->getBirthdayFromPesel();
    }

    /**
     * Check if PESEL is correct
     * @see PeselValidator::isPeselLenghtCorrect()
     * @see PeselValidator::isPeselANumber()
     * @see PeselValidator::isValidPesel()
     * @return bool
     */
    public function isPeselCorrect() : bool
    {
        return $this->isValidPesel();
    }

    /**
     * Valid correct "PESEL"
     *
     * @return bool
     */
    private function isValidPesel() : bool
    {
        $sum = 0;
        foreach(self::PESEL_WEIGHT as $item=>$value){

            $sum += $value * $this->peselNumber[$item];
        }
        $mod = $sum % 10;
        return $mod == 0 && $sum>0;
    }

}